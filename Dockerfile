FROM openjdk:8u171-jdk-alpine3.8
LABEL descrption="This image provides a minumal Linux setup to perform SBT builds of JVM applications"
LABEL maintainer="paolo.angioletti@gmail.com"

ENV DOCKER_COMPOSE_VERSION=1.22.0 \
    SBT_VERSION=1.2.3 \
    PATH=${PATH}:/opt/sbt/bin

RUN set -ex \
 && apk -U upgrade \
 && apk add curl tar gzip \
 && curl -OL https://github.com/sbt/sbt/releases/download/v${SBT_VERSION}/sbt-${SBT_VERSION}.tgz \
 && mkdir -p /opt \
 && tar xvfz sbt-${SBT_VERSION}.tgz -C /opt \
 && rm -f sbt-${SBT_VERSION}.tgz \
 && mv /opt/sbt/ /opt/sbt-${SBT_VERSION} \
 && ln -s sbt-${SBT_VERSION} /opt/sbt \
 && curl -L https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > docker-compose \
 && chmod +x docker-compose \
 && mv docker-compose /usr/local/bin  \
 && apk del curl tar gzip \
 && rm -Rf /tmp/* /var/cache/apk/*

ENTRYPOINT []
