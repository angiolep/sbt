# sbt
This Docker image provides a minimal Linux setup to perform SBT builds of JVM applications within a Docker container.

This image provides:

* [Linux Alpine](https://www.alpinelinux.org) >= 3.8
* [SBT - the interactive build tool](https://www.scala-sbt.org) >= 1.2.3
* [Open JDK](http://openjdk.java.net/) >= 1.8.0_172
* [GNU Bash](https://www.gnu.org/software/bash/) >= 4.4.19

## build & publish
Login to your GitLab user account, build the Docker image and the publish it for other developers to enjoy.

```
docker login \
  --username $GITLAB_USERNAME \
  registry.alpinedata.tech

docker image build \
  --pull \
  --tag registry.alpinedata.tech/docker/sbt:1.2.3 \
  .

docker image push \
  registry.alpinedata.tech/docker/sbt:1.2.3
```
